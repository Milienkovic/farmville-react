import React, {Fragment, useEffect} from 'react';
import './App.css';
import {Redirect, Route, Switch} from "react-router-dom";
import Urls from "./utils/urls";
import Home from "./ui/pages/Home/Home";
import MainNavigation from "./ui/components/commons/MainNavigation/MainNavigation";
import Login from "./ui/components/auth/Login/Login";
import Registration from "./ui/components/auth/Registration/Registration";
import CreateCustomer from "./ui/components/customers/CreateCustomer/CreateCustomer";
import CreateFarm from "./ui/components/farms/CreateFarm/CreateFarm";
import UpdateUser from "./ui/components/users/UpdateUser/UpdateUser";
import {useDispatch, useSelector} from "react-redux";
import {useToken} from "./ui/hooks/useToken";
import {loginSuccess, logout} from "./store/actions/auth/authActions";
import Accounts from "./ui/components/accounts/Accounts/Accounts";
import Farms from "./ui/components/farms/Farms/Farms";
import UpdateFarm from "./ui/components/farms/UpdateFarm/UpdateFarm";
import Customers from "./ui/components/customers/Customers/Customers";
import Users from "./ui/components/users/Users/Users";
import AdminPanel from "./ui/pages/AdminPanel/AdminPanel";
import UpdateCustomer from "./ui/components/customers/UpdateCustomer/UpdateCustomer";
import AvailableFarms from "./ui/components/farms/Farms/AvailableFarms";
import AvailableAccounts from "./ui/components/accounts/Accounts/AvailableAccounts";
import TestingAccounts from "./ui/components/accounts/Accounts/TestingAccounts";
import AdminRoute from "./ui/components/commons/routes/AdminRoute";
import AuthenticatedRoute from "./ui/components/commons/routes/AuthenticatedRoute";

function App() {
    const dispatch = useDispatch();
    const {isTokenStored, isTokenValid, getAuthToken} = useToken();
    const {isAuth} = useSelector(state => ({
        isAuth: !!state.auth.token
    }));

    useEffect(() => {
        if (!isAuth && isTokenStored()) {
            const token = getAuthToken();
            if (isTokenValid(token)) {
                dispatch(loginSuccess(token));
            } else {
                dispatch(logout());
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <Fragment>
            <MainNavigation/>
            <main className="App">
                <Switch>
                    <Route exact path={Urls.HOME} component={Home}/>
                    <AdminRoute exact path={Urls.ADMIN_PANEL} component={AdminPanel}/>

                    <Route exact path={Urls.LOGIN} component={Login}/>
                    <Route exact path={Urls.REGISTRATION} component={Registration}/>

                    <AdminRoute exact path={Urls.CUSTOMERS} component={Customers}/>
                    <AdminRoute exact path={Urls.CREATE_CUSTOMER} component={CreateCustomer}/>
                    <AdminRoute exact path={Urls.UPDATE_CUSTOMER} component={UpdateCustomer}/>

                    <AuthenticatedRoute exact path={Urls.UPDATE_USER} component={UpdateUser}/>
                    <AdminRoute exact path={Urls.USERS} component={Users}/>

                    <AdminRoute exact path={Urls.FARMS} component={Farms}/>
                    <AuthenticatedRoute exact path={Urls.AVAILABLE_FARMS} component={AvailableFarms}/>
                    <AdminRoute exact path={Urls.CREATE_FARM} component={CreateFarm}/>
                    <AuthenticatedRoute exact path={Urls.ACCOUNT_FARMS} component={Farms}/>
                    <AdminRoute exact path={Urls.UPDATE_FARM} component={UpdateFarm}/>

                    <AdminRoute exact path={Urls.ACCOUNTS} component={Accounts}/>
                    <AuthenticatedRoute exact path={Urls.AVAILABLE_ACCOUNTS} component={AvailableAccounts}/>
                    <Route exact path={Urls.ACCOUNTS_TESTING} component={TestingAccounts}/>

                    <Redirect to={Urls.HOME}/>
                </Switch>
            </main>
        </Fragment>
    );
}

export default App;
