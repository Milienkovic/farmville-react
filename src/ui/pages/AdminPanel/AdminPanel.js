import React from 'react';
import Button from "../../components/commons/form/Button/Button";
import Urls from "../../../utils/urls";

import './AdminPanel.css';

const AdminPanel = () => {
    return (
        <div className={'admin_container'}>
            <div className={'admin_inner-container'}>
                <Button success to={Urls.USERS}>Users</Button>
                <Button success to={Urls.CUSTOMERS}>Customers</Button>
                <Button success to={Urls.ACCOUNTS}>Accounts</Button>
                <Button success to={Urls.FARMS}>Farms</Button>
            </div>
        </div>
    );
};

export default AdminPanel;