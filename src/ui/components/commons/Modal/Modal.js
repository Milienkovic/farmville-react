import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import Backdrop from "./Backdrop/Backdrop";

import "./Modal.css";

const ModalOverlay = props => {
    const getClassNames = () => {
        const classNames = ['modal'];
        if (props.className)
            classNames.push(props.className)
        if (props.center)
            classNames.push('modal--content__center')
        return classNames;
    }

    const getHeaderClasses = () => {
        const headerClassNames = ['modal__header'];
        if (props.headerClass)
            headerClassNames.push(props.headerClass);
        if (props.danger && !props.info)
            headerClassNames.push('modal__header__danger');
        if (props.info && !props.danger)
            headerClassNames.push('modal__header__info');
        if (props.warning)
            headerClassNames.push('modal__header__warning')
        return headerClassNames;
    }
    const content = (
        <div className={getClassNames().join(' ')} style={props.style}>
            <header className={getHeaderClasses().join(' ')}
            >
                <h2>{props.header}</h2>
            </header>
            <div>
                <div className={`modal__content ${props.contentClass}`}>{props.children}</div>
                <footer className={`modal__footer ${props.footerClass}`}>
                    {props.footer}
                </footer>
            </div>
        </div>
    );
    return ReactDOM.createPortal(content, document.getElementById('modal-hook'));
}

const Modal = (props) => {

    const renderModal = () => {
        if (props.displayModal) {
            return (
                <Fragment>
                    <Backdrop display={props.displayModal} clicked={props.closeModal}/>
                    <ModalOverlay {...props}/>
                </Fragment>
            )
        }
    };
    return (
        <Fragment>
            {renderModal()}
        </Fragment>
    );
};

export default Modal;