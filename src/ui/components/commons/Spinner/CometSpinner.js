import React, {Fragment} from 'react';

import './CometSpinner.css';

const CometSpinner = (props) => {
    return (
        <Fragment>
            {props.display && <div className={'comet--container'}>
                <div className="comet--loader">
                    <div className="comet--face">
                        <div className="comet--circle"/>
                    </div>
                    <div className="comet--face">
                        <div className="comet--circle"/>
                    </div>
                </div>
            </div>}
        </Fragment>
    );
};

export default CometSpinner;