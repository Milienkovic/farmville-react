import React, {useEffect, useReducer} from 'react';

import './Input.css';

const CHANGE = 'CHANGE';

const inputReducer = (state, action) => {

    switch (action.type) {

        case CHANGE:
            return {
                ...state,
                value: action.value
            }

        default:
            return state;
    }

}

const Input = (props) => {
    const [inputState, dispatch] = useReducer(inputReducer, {
        value: props.initValue || '',
    });

    const {onInput, id} = props;

    const changeHandler = event => {
        const {value} = event.target;
        dispatch({type: CHANGE, value});
    }

    useEffect(() => {
        onInput(id, inputState.value);
    }, [id, inputState.value, onInput]);

    const labelText = () => {
        if (props.fieldError) {
            return props.label + ":     " + props.fieldError;
        } else
            return props.label;
    }

    return (
        <div className={'input_group'}>
            <label htmlFor={props.id} className={props.fieldError && 'input_error-label'}>{labelText()}</label>
            <input id={props.id}
                   name={props.name}
                   value={inputState.value}
                   onChange={changeHandler}
                   type={props.type}
                   autoFocus={props.autoFocus}
                   min={props.min}
                   step={props.step}
                   style={props.fieldError ? {borderBottom: '2px solid darkred'} : {}}
            />
        </div>
    );
};

export default Input;