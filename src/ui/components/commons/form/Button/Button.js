import React from 'react';

import './Button.css';
import {Link} from "react-router-dom";

const Button = props => {

    const getClassNames = () => {
        const classNames = ['button'];
        if (props.inverse) {
            classNames.push('button-inverse')
        }
        if (props.danger) {
            classNames.push('button-danger');
        }
        if (props.primary) {
            classNames.push('button-primary');
        }
        if (props.success){
            classNames.push('button-success');
        }
        if (props.dark){
            classNames.push('button-dark');
        }

        if (props.warning){
            classNames.push('button-warning');
        }
        return classNames;
    }

    if (props.hasOwnProperty('to')){
        return <Link to={props.to} exact={props.exact} className={getClassNames().join(' ')}>
            {props.children}
        </Link>
    }

    return (
        <button onClick={props.onClick}
                type={props.type}
                className={getClassNames().join(' ')}>
            {props.children}
        </button>
    );
};

export default Button;