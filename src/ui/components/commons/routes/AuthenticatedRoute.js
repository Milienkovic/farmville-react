import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route} from "react-router-dom";
import Urls from "../../../../utils/urls";

const AuthenticatedRoute = ({component: Component, ...otherProps}) => {
    const {isAuth} = useSelector(state => ({
        isAuth: state.auth.token
    }))
    return (
        <Route {...otherProps}
               render={props =>
                   isAuth ?
                       <Component {...props}/>
                       : <Redirect to={Urls.HOME}/>}
        />
    );
};

export default AuthenticatedRoute;