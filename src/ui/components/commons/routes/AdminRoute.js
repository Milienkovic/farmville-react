import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route} from "react-router-dom";
import Urls from "../../../../utils/urls";

const AdminRoute = ({component: Component, ...otherProps}) => {
    const {isAdmin} = useSelector(state => ({
        isAdmin: state.auth.roles && state.auth.roles.includes('ADMIN')
    }));

    return (
        <Route {...otherProps}
               render={props => isAdmin ?
                   <Component {...props}/>
                   : <Redirect to={Urls.HOME}/>}
        />
    );
};

export default AdminRoute;