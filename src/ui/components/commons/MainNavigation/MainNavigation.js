import React from 'react';
import NavigationItems from "./NavigationItems/NavigationItems";
import Urls from "../../../../utils/urls";
import {Link} from "react-router-dom";

import './MainNavigation.css';
const MainNavigation = () => {
    return (
            <header className={'main-navigation_container'}>
                <div className=''><Link to={Urls.HOME}><span>Home</span></Link></div>
                <div className='spacer'/>
                <div className=''>
                    <nav>
                        <NavigationItems/>
                    </nav>
                </div>
            </header>
    );
};

export default MainNavigation;