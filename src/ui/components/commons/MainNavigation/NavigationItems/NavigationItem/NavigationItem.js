import React, {Fragment} from 'react';
import {NavLink} from "react-router-dom";

import './NavigationItem.css';
const NavigationItem = props => {
    return (
        <Fragment>
            <NavLink to={props.to}
                     onClick={props.onClick}
                     activeClassName={'active-navigation'}
                     className={'navigation_item'}
                     exact={props.exact}
            >{props.children}</NavLink>
        </Fragment>
    );
};

export default NavigationItem;