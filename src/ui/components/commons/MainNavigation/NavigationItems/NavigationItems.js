import React, {Fragment} from 'react';
import {useDispatch, useSelector} from "react-redux";
import Urls from "../../../../../utils/urls";
import {logout} from "../../../../../store/actions/auth/authActions";
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    const dispatch = useDispatch();
    const {isAdmin, isAuth} = useSelector(state => ({
        isAdmin: state.auth.roles && state.auth.roles.includes('ADMIN'),
        isAuth: !!state.auth.token
    }));

    const renderAuthenticatedContent = () => (
        !isAuth ?
            <Fragment>
                <NavigationItem to={Urls.LOGIN} exact>Login</NavigationItem>
            </Fragment>
            :
            <Fragment>
                <NavigationItem to={Urls.UPDATE_USER} exact>Update User</NavigationItem>
                <NavigationItem to={Urls.AVAILABLE_ACCOUNTS} exact>Accounts</NavigationItem>
                <NavigationItem to={Urls.AVAILABLE_FARMS} exact>Farms</NavigationItem>
                <NavigationItem to={Urls.ACCOUNTS_TESTING} exact>Test Accounts</NavigationItem>
                <NavigationItem to={Urls.LOGOUT} exact onClick={() => dispatch(logout())}>Logout</NavigationItem>
            </Fragment>
    );

    const renderAdminContent = () => {
        if (isAdmin) {
            return (
                <Fragment>
                    <NavigationItem to={Urls.ADMIN_PANEL} exact>Admin Panel</NavigationItem>
                </Fragment>
            )
        }
    }
    return (
        <ul className='navigation-items'>
            {renderAdminContent()}
            {renderAuthenticatedContent()}
            <div className={'actions'}>
            </div>
        </ul>
    );
};

export default NavigationItems;