import React, {useEffect} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import Input from "../../commons/form/Input/Input";
import Button from "../../commons/form/Button/Button";
import {useForm} from "../../../hooks/useForm";
import CometSpinner from "../../commons/Spinner/CometSpinner";
import {createCustomer, fetchCustomer, updateCustomer} from "../../../../store/actions/customers/customerActions";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Urls from "../../../../utils/urls";

import './CustomerForm.css';

const initState = {
    firstName: '',
    lastName: '',
    address: '',
    email: '',
    phone: '',
    funds: 0.0,
    accountNumber: new Date().getMilliseconds() * 333
}

const CustomerForm = (props) => {
    const [customerForm, inputHandler, setFormData] = useForm(initState);
    const dispatch = useDispatch();
    const history = useHistory();
    const customerId = useParams().customerId;
    const {loading, error, fieldErrors, customer} = useSelector(state => ({
        loading: state.customers.loading,
        error: state.customers.error,
        fieldErrors: state.customers.fieldErrors,
        customer: state.customers.customer
    }));

    useEffect(() => {
        if (props.updating && customerId) {
            dispatch(fetchCustomer(customerId));
        }
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (props.updating && customer && +customerId === customer.id) {
            const customerData = {
                firstName: customer.firstName,
                lastName: customer.lastName,
                address: customer.address,
                email: customer.email,
                phone: customer.phone,
                funds: customer.account.funds,
                accountNumber: customer.account.accountNumber
            }
            setFormData(customerData);
        }
    }, [customer, customerId, props.updating, setFormData]);

    const submitHandler = event => {
        event.preventDefault();
        const {firstName, lastName, address, phone, email, accountNumber, funds} = customerForm.inputs;
        console.log(customerForm.inputs)
        const customer = {
            firstName,
            lastName,
            address,
            phone,
            email,
            account: {
                accountNumber,
                funds
            }
        }
        if (props.updating) {
            dispatch(updateCustomer(customerId, customer, history, Urls.CUSTOMERS));
        } else {
            dispatch(createCustomer(customer, history));
        }
    }

    if (loading || (props.updating && !customer) || (props.updating && customer && customerForm.inputs === initState)) {
        return <CometSpinner display={loading}/>
    }
    return (
        <div className={'customer_container'}>
            <div className={'customer_inner-container'}>
                {error && <h3 className={'customer-error'}>{error}</h3>}
                <form onSubmit={submitHandler}>
                    <Input
                        id={'firstName'}
                        name={'firstName'}
                        type={'text'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.firstName && fieldErrors.firstName}
                        initValue={customerForm.inputs.firstName}
                        autoFocus
                        label={'First Name'}
                    />
                    <Input
                        id={'lastName'}
                        name={'lastName'}
                        type={'text'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.lastName && fieldErrors.lastName}
                        initValue={customerForm.inputs.lastName}
                        label={'Last Name'}
                    />
                    <Input
                        id={'email'}
                        name={'email'}
                        type={'email'}
                        label={'Email'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                        initValue={customerForm.inputs.email}
                    />
                    <Input
                        id={'address'}
                        name={'address'}
                        type={'text'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.address && fieldErrors.address}
                        initValue={customerForm.inputs.address}
                        label={'Address'}
                    />
                    <Input
                        id={'phone'}
                        name={'phone'}
                        type={'phone'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.phone && fieldErrors.phone}
                        initValue={customerForm.inputs.phone}
                        label={'Phone'}
                    />
                    <Input
                        id={'accountNumber'}
                        name={'accountNumber'}
                        type={'number'}
                        min={0}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.accountNumber && fieldErrors.accountNumber}
                        initValue={customerForm.inputs.accountNumber}
                        label={'Account Number'}
                    />
                    <Input
                        id={'funds'}
                        name={'funds'}
                        min={0}
                        step={0.01}
                        type={'number'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.funds && fieldErrors.funds}
                        initValue={customerForm.inputs.funds}
                        label={'Account funds'}
                    />
                    <Button type={'submit'}>Submit</Button>
                </form>
            </div>
        </div>
    );
};

export default CustomerForm;