import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import CometSpinner from "../../commons/Spinner/CometSpinner";
import Customer from "./Customer/Customer";
import {fetchCustomers} from "../../../../store/actions/customers/customerActions";
import Button from "../../commons/form/Button/Button";
import Urls from "../../../../utils/urls";

import './Customers.css';

const Customers = () => {

    const dispatch = useDispatch();
    const {loading, customers} = useSelector(state => ({
        loading: state.customers.loading,
        customers: state.customers.customers
    }));

    useEffect(() => {
        dispatch(fetchCustomers());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loading || !customers) {
        return <CometSpinner display={loading}/>
    }

    const renderCustomers = () => {
        const customersCollection = Object.values(customers);
        if (customersCollection.length === 0) {
            return <h1>No Customers Available</h1>
        }
        return customersCollection.map(customer => (
            <Customer key={customer.id} customer={customer}/>
        ));
    }
    return (
        <div className={'customers_container'}>
            <div className={'customers_inner-container'}>
                <Button success to={Urls.CREATE_CUSTOMER}> Create Customer</Button>
                {renderCustomers()}
            </div>
        </div>
    );
};

export default Customers;