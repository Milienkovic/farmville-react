import React, {useState} from 'react';
import Button from "../../../commons/form/Button/Button";
import Modal from "../../../commons/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import {deleteCustomer} from "../../../../../store/actions/customers/customerActions";
import CometSpinner from "../../../commons/Spinner/CometSpinner";
import Urls from "../../../../../utils/urls";

import './Customer.css';

const Customer = ({customer}) => {
    const [displayModal, setDisplayModal] = useState(false);

    const dispatch = useDispatch();
    const {loading} = useSelector(state => ({
        loading: state.customers.loading
    }));

    const modalCloseHandler = () => {
        setDisplayModal(false);
    }

    const modalOpenHandler = () => {
        setDisplayModal(true);
    }

    const deleteCustomerHandler = () => {
        setDisplayModal(false);
        dispatch(deleteCustomer(customer.id));
    }

    const modalActions = () => {
        return (
            <div>
                <Button dark onClick={modalCloseHandler}>Cancel</Button>
                <Button danger onClick={deleteCustomerHandler}>Delete</Button>
            </div>
        )
    }

    if (loading) {
        return <CometSpinner display={loading}/>
    }

    return (
        <div className={'customer_preview-container'}>
            <div className={'customer_preview-inner-container'}>
                <span>Name: {customer.firstName} {customer.lastName}</span>
                <span>Address: {customer.address}</span>
                <span>Phone: {customer.phone}</span>
                <div className={'customer_actions'}>
                    <Button warning to={Urls.UPDATE_CUSTOMER_URL(customer.id)}>Edit</Button>
                    <Button danger onClick={modalOpenHandler}>delete</Button>
                </div>
            </div>
            <Modal displayModal={displayModal}
                   closeModal={modalCloseHandler}
                   header={'Delete Customer'}
                   danger
                   footer={modalActions()}>
                Are you sure you want to delete this customer?
            </Modal>
        </div>
    );
};

export default Customer;