import React, {Fragment} from 'react';
import CustomerForm from "../CustomerForm/CustomerForm";

const CreateCustomer = () => {
    return (
        <Fragment>
            <CustomerForm/>
        </Fragment>
    );
};

export default CreateCustomer;