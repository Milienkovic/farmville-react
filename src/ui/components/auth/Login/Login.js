import React, {useEffect} from 'react';

import {Link} from "react-router-dom";
import Urls from "../../../../utils/urls";
import Input from "../../commons/form/Input/Input";
import Button from "../../commons/form/Button/Button";
import {useForm} from "../../../hooks/useForm";
import {useDispatch, useSelector} from "react-redux";
import {login} from "../../../../store/actions/auth/authActions";
import CometSpinner from "../../commons/Spinner/CometSpinner";
import {useHistory} from 'react-router-dom';
import './Login.css';

const initState = {
    email: '',
    password: ''
}

const Login = () => {
    const [formState, inputHandler] = useForm(initState);
    const dispatch = useDispatch();
    const history = useHistory();
    const {loading, error, fieldErrors, isAuthenticated} = useSelector(state => ({
        loading: state.auth.loading,
        error: state.auth.error,
        fieldErrors: state.auth.fieldErrors,
        isAuthenticated: !!state.auth.token
    }));

    useEffect(() => {
        if (isAuthenticated) {
            history.push(Urls.HOME);
        }
    }, [history, isAuthenticated])

    const submitHandler = event => {
        event.preventDefault();
        dispatch(login({email: formState.inputs.email, password: formState.inputs.password}));
    }

    if (loading) {
        return <CometSpinner display={loading}/>
    }

    return (
        <div className={'login_container'}>
            <div className={'login_inner-container'}>
                {error && <h3 className={'login_failed'}>{error}</h3>}
                <form onSubmit={submitHandler} className={'login_form'}>
                    <Input
                        id={'email'}
                        name={'email'}
                        type={'text'}
                        label={'Email'}
                        initValue={formState.inputs.email}
                        fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                        autoFocus
                        onInput={inputHandler}
                    />
                    <Input
                        id={'password'}
                        name={'password'}
                        type={'password'}
                        label={'Password'}
                        onInput={inputHandler}
                        fieldError={fieldErrors && fieldErrors.password && fieldErrors.password}
                        initValue={formState.inputs.password}
                    />
                    <Button type={'submit'}>Login</Button>
                </form>
                <Link to={Urls.REGISTRATION}>Don't have account?</Link>
            </div>
        </div>
    );
};

export default Login;