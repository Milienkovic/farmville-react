import React from 'react';

import {Link} from "react-router-dom";
import Urls from "../../../../utils/urls";
import Input from "../../commons/form/Input/Input";
import Button from "../../commons/form/Button/Button";
import {useForm} from "../../../hooks/useForm";
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';

import './Registration.css';
import CometSpinner from "../../commons/Spinner/CometSpinner";
import {registration} from "../../../../store/actions/auth/authActions";

const initState = {
    email: '',
    password: '',
    confirmPassword: ''
}

const Registration = () => {

    const [registrationForm, inputHandler] = useForm(initState);
    const dispatch = useDispatch();
    const {loading, fieldErrors, error} = useSelector(state => ({
        loading: state.auth.loading,
        fieldErrors: state.auth.fieldErrors,
        error: state.auth.error
    }));
    const history = useHistory();

    const submitHandler = event => {
        event.preventDefault();
        dispatch(registration(registrationForm.inputs, history));
        console.log(registrationForm.inputs)
    }

    if (loading) {
        return <CometSpinner display={loading}/>
    }

    return (
        <div className={'registration_container'}>
            <div className={'registration_inner-container'}>
                {error && <h3 className={'registration-error'}>{error}</h3>}
                <form onSubmit={submitHandler} className={'registration_form'}>
                    <Input
                        id={'email'}
                        name={'email'}
                        initValue={registrationForm.inputs.email}
                        fieldError={fieldErrors && fieldErrors.email && fieldErrors.email}
                        label={'Email'}
                        onInput={inputHandler}
                        type={'email'}
                        autoFocus
                    />
                    <Input
                        id={'password'}
                        name={'password'}
                        type={'password'}
                        initValue={registrationForm.inputs.password}
                        fieldError={fieldErrors && fieldErrors.password && fieldErrors.password}
                        onInput={inputHandler}
                        label={'Password'}

                    />
                    <Input
                        id={'confirmPassword'}
                        name={'confirmPassword'}
                        type={'password'}
                        initValue={registrationForm.inputs.confirmPassword}
                        fieldError={fieldErrors && fieldErrors.confirmPassword && fieldErrors.confirmPassword}
                        label={'Confirm Password'}
                        onInput={inputHandler}
                    />
                    <Button type='submit'>Register</Button>
                </form>
                <span>Back to<Link to={Urls.LOGIN}> Login</Link></span>
            </div>
        </div>
    );
};

export default Registration;