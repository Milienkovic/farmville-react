import React from 'react';
import Urls from "../../../../../utils/urls";
import Button from "../../../commons/form/Button/Button";

import './Account.css';

const Account = ({account, actions}) => {
    return (
        <div className={'account_container'}>
            <span>Account number: {account.accountNumber}</span>
            <span>Available funds: {account.funds}</span>
            <div className={'account_actions'}>
                <Button to={Urls.ACCOUNT_FARMS_URL(account.id)} success>Farms</Button>
                {actions}
            </div>
        </div>
    );
};

export default Account;