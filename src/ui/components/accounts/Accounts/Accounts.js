import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import CometSpinner from "../../commons/Spinner/CometSpinner";
import {
    fetchAccounts,
    fetchUserAccessibleAccounts,
    grantAccess,
    revokeAccess
} from "../../../../store/actions/accounts/accountActions";
import Account from "./Account/Account";
import Button from "../../commons/form/Button/Button";

import './Accounts.css';

const Accounts = ({available, testing}) => {
    const [indices, setIndices] = useState([]);
    const dispatch = useDispatch();
    const {loading, accounts, availableAccounts} = useSelector(state => ({
        loading: state.accounts.loading,
        accounts: state.accounts.accounts,
        error: state.accounts.error,
        availableAccounts: state.accounts.availableAccounts,
    }));

    useEffect(() => {
        if (testing && availableAccounts) {
            setIndices(Object.keys(availableAccounts).map(Number));
        }
    }, [availableAccounts, testing]);

    useEffect(() => {
        if (testing) {
            dispatch(fetchUserAccessibleAccounts());
            dispatch(fetchAccounts());
        } else if (available) {
            dispatch(fetchUserAccessibleAccounts());
        } else {
            dispatch(fetchAccounts());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const grantAccessHandler = (accountId) => {
        setIndices(prevState => [...prevState, accountId]);
        dispatch(grantAccess(accountId));
    }

    const revokeAccessHandler = (accountId) => {
        const temp = indices.filter(value => value !== accountId);
        setIndices(temp);
        dispatch(revokeAccess(accountId));
    }

    if (loading || (!available && !accounts) || (available && !availableAccounts) || (testing && !accounts && !availableAccounts)) {
        return <CometSpinner display={loading}/>
    }

    const actions = (accountId) => {
        if (testing && availableAccounts) {
            if (indices.includes(accountId)) {
                return <Button
                    onClick={() => revokeAccessHandler(accountId)}
                    danger>Revoke</Button>
            } else {
                return <Button
                    onClick={() => grantAccessHandler(accountId)}
                    success>Grant</Button>
            }
        }
    }

    const renderAccounts = () => {
        let accountCollection;
        if (!available) {
            accountCollection = Object.values(accounts);
        } else {
            accountCollection = Object.values(availableAccounts);
        }
        if (accountCollection.length === 0) {
            return <h1>No Accounts Available</h1>
        }

        return accountCollection.map(account =>
            <Account account={account}
                     key={account.id}
                     available={available}
                     testing={testing}
                     actions={actions(account.id)}
            />)
    }

    return (
        <div className={'accounts_container'}>
            <div className={'accounts_inner-container'}>
                {renderAccounts()}
            </div>
        </div>
    );
};

export default Accounts;