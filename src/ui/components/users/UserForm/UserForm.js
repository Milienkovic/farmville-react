import React, {Fragment, useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';
import Input from "../../commons/form/Input/Input";
import Button from "../../commons/form/Button/Button";
import {deleteUser, fetchUserByEmail, updateUser} from "../../../../store/actions/users/userActions";
import {useForm} from "../../../hooks/useForm";
import './UserForm.css';
import CometSpinner from "../../commons/Spinner/CometSpinner";
import {useToken} from "../../../hooks/useToken";
import {purgeErrors} from "../../../../store/actions/general/generalActions";
import Modal from "../../commons/Modal/Modal";

const initState = {
    firstName: '',
    lastName: '',
    address: '',
    phone: '',
}

const UserForm = (props) => {
    const [displayModal, setDisplayModal] = useState(false);
    const dispatch = useDispatch();
    const history = useHistory();
    const {getAuthToken, decodeAuthToken, isTokenValid, isTokenStored} = useToken();
    const [userForm, inputHandler, setFormData] = useForm(initState);

    const {loading, error, fieldErrors, user} = useSelector(state => ({
        loading: state.users.loading,
        error: state.users.error,
        fieldErrors: state.users.fieldErrors,
        user: state.users.user
    }));

    useEffect(() => {
        if (isTokenStored()) {
            const token = getAuthToken();
            if (isTokenValid(token)) {
                const decodedToken = decodeAuthToken(token);
                dispatch(fetchUserByEmail(decodedToken.sub));
            }
        }

        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (user && userForm.inputs === initState) {
            setFormData(user);
        }
    }, [setFormData, user, userForm.inputs]);

    const submitHandler = event => {
        event.preventDefault();
        console.log(userForm.inputs)
        dispatch(updateUser(user.id, userForm.inputs, history));
    }

    const displayModalHandler = () => {
        setDisplayModal(true);
    }

    const closeModalHandler = () => {
        setDisplayModal(false);
    }

    const deleteAccountHandler = () => {
        setDisplayModal(false)
        dispatch(deleteUser(user.id, history));
    }

    const modalActions = () => {
        return <div>
            <Button dark onClick={closeModalHandler}>Cancel</Button>
            <Button danger onClick={deleteAccountHandler}>Delete</Button>
        </div>
    }

    if (loading || (!user && props.updating) || (user && userForm.inputs === initState)) {
        return <CometSpinner display={loading}/>
    }

    return (
        <Fragment>
            <div className={'user_container'}>
                <div className={'user_inner-container'}>
                    {error && <h3 className={'user-error'}>{error}</h3>}
                    <form onSubmit={submitHandler}>
                        <Input
                            id={'firstName'}
                            name={'firstName'}
                            type={'text'}
                            initValue={userForm.inputs.firstName}
                            onInput={inputHandler}
                            fieldError={fieldErrors && fieldErrors.firstName && fieldErrors.firstName}
                            label={'First Name'}
                            autoFocus
                        />
                        <Input
                            id={'lastName'}
                            name={'lastName'}
                            type={'text'}
                            initValue={userForm.inputs.lastName}
                            onInput={inputHandler}
                            fieldError={fieldErrors && fieldErrors.lastName && fieldErrors.lastName}
                            label={'Last Name'}
                        />
                        <Input
                            id={'address'}
                            name={'address'}
                            type={'text'}
                            initValue={userForm.inputs.address}
                            onInput={inputHandler}
                            fieldError={fieldErrors && fieldErrors.address && fieldErrors.address}
                            label={'Address'}
                        />
                        <Input
                            id={'phone'}
                            name={'phone'}
                            type={'text'}
                            initValue={userForm.inputs.phone}
                            onInput={inputHandler}
                            fieldError={fieldErrors && fieldErrors.phone && fieldErrors.phone}
                            label={'Phone'}
                        />
                        <Button type={'submit'}>Submit</Button>
                        <Button danger type={'button'} onClick={displayModalHandler}>Delete Account</Button>
                    </form>
                </div>
            </div>
            <Modal displayModal={displayModal}
                   header={'Delete Account'}
                   danger
                   footer={modalActions()}
                   closeModal={closeModalHandler}>
                Are you sure you want to delete your account?
            </Modal>
        </Fragment>
    );
};

export default UserForm;