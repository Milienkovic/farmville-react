import React from 'react';

import './User.css';

const User = ({user}) => {
    return (
        <div className={'user_preview-container'}>
            <div className={'user_preview-inner-container'}>
                <span>Name: {user.firstName} {user.lastName}</span>
                <span>Address: {user.address}</span>
                <span>Email: {user.email}</span>
                <span>Phone: {user.phone}</span>
            </div>
        </div>
    );
};

export default User;