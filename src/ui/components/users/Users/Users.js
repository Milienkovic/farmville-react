import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import CometSpinner from "../../commons/Spinner/CometSpinner";


import './Users.css';
import {fetchUsers} from "../../../../store/actions/users/userActions";
import User from "./User/User";

const Users = () => {
    const dispatch = useDispatch();
    const {loading, users} = useSelector(state => ({
        loading: state.users.loading,
        users: state.users.users
    }));

    useEffect(() => {
        dispatch(fetchUsers());
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    if (loading || !users) {
        return <CometSpinner display={loading}/>
    }

    const renderUsers = () => {
        const usersCollection = Object.values(users);
        if (usersCollection.length === 0) {
            return <h1>No Users Available!</h1>
        }
        return usersCollection.map(user => (
            <User key={user.id} user={user}/>
        ))
    }
    return (
        <div className={'users_container'}>
            <div className={'users_inner-container'}>
                {renderUsers()}
            </div>
        </div>
    );
};

export default Users;