import React from 'react';
import UserForm from "../UserForm/UserForm";

import './UpdateUser.css';

const UpdateUser = () => {
    return (
        <div className={'update_user_container'}>
            <div className={'update_user_inner-container'}>
                <UserForm updating/>
            </div>
        </div>
    );
};

export default UpdateUser;