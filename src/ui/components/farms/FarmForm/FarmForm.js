import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from 'react-router-dom';
import Input from "../../commons/form/Input/Input";
import Button from "../../commons/form/Button/Button";
import {useForm} from "../../../hooks/useForm";
import CometSpinner from "../../commons/Spinner/CometSpinner";
import {createFarm, fetchFarm, updateFarm} from "../../../../store/actions/farms/farmActions";
import Urls from "../../../../utils/urls";
import {purgeErrors} from "../../../../store/actions/general/generalActions";

import './FarmForm.css';

const initState = {
    name: '',
    address: ''
}

const FarmForm = (props) => {
    const [farmForm, inputHandler, setFormData] = useForm(initState);
    const dispatch = useDispatch();
    const history = useHistory();
    const accountId = useParams().accountId;
    const farmId = useParams().farmId;

    const {loading, error, fieldErrors, farm} = useSelector(state => ({
        loading: state.farms.loading,
        error: state.farms.error,
        fieldErrors: state.farms.fieldErrors,
        farm: state.farms.farm
    }));

    useEffect(() => {
        if (props.updating && farmId) {
            dispatch(fetchFarm(farmId));
        }
        return () => {
            dispatch(purgeErrors());
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    useEffect(() => {
        if (props.updating && farm && +farmId === farm.id) {
            const farmData = {
                name: farm.name,
                address: farm.address
            }
            setFormData(farmData);
        }
    }, [farm, farmId, props.updating, setFormData]);

    const submitHandler = event => {
        event.preventDefault();
        if (props.updating) {
            dispatch(updateFarm(farmId, farmForm.inputs, history, Urls.HOME));
        } else {
            dispatch(createFarm(accountId, farmForm.inputs, history, Urls.ACCOUNT_FARMS_URL(accountId)));
        }
    }

    if (loading || (props.updating && !farm) || (props.updating && farm && farmForm.inputs === initState)) {
        return <CometSpinner display={loading}/>
    }

    return (
        <div className={'farm_container'}>
            <div className={'farm_inner-container'}>
                {error && <h3 className={'farm-error'}>{error}</h3>}
                <form onSubmit={submitHandler}>
                    <Input
                        id={'name'}
                        name={'name'}
                        type={'text'}
                        onInput={inputHandler}
                        initValue={farmForm.inputs.name}
                        fieldError={fieldErrors && fieldErrors.name && fieldErrors.name}
                        label={'Farm Name'}
                        autoFocus
                    />
                    <Input
                        id={'address'}
                        name={'address'}
                        type={'text'}
                        onInput={inputHandler}
                        initValue={farmForm.inputs.address}
                        fieldError={fieldErrors && fieldErrors.address && fieldErrors.address}
                        label={'Farm Address'}
                    />
                    <Button type={'submit'}>Submit</Button>
                </form>
            </div>
        </div>
    );
};

export default FarmForm;