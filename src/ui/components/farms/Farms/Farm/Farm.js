import React, {Fragment, useState} from 'react';
import Button from "../../../commons/form/Button/Button";
import Urls from "../../../../../utils/urls";
import Modal from "../../../commons/Modal/Modal";
import {useDispatch, useSelector} from "react-redux";
import CometSpinner from "../../../commons/Spinner/CometSpinner";
import {deleteFarm} from "../../../../../store/actions/farms/farmActions";

import './Farm.css';

const Farm = ({farm, isAdmin}) => {
    const [displayModal, setDisplayModal] = useState(false);
    const [farmId, setFarmId] = useState();
    const dispatch = useDispatch();

    const {loading} = useSelector(state => ({
        loading: state.farms.loading
    }));

    const handleModal = (farmId) => {
        setFarmId(farmId);
        setDisplayModal(true);
    }

    const handleModalClosing = () => {
        setDisplayModal(false);
    }

    const handleFarmDeletion = () => {
        setDisplayModal(farm);
        if (farmId)
            dispatch(deleteFarm(farmId));
    }

    const modalActions = () => {
        return <div>
            <Button onClick={handleModalClosing} type={'button'} dark>Cancel</Button>
            <Button onClick={handleFarmDeletion} danger>Delete</Button>
        </div>
    }

    if (loading) {
        return <CometSpinner display={loading}/>
    }
    return (
        <Fragment>
            <div className={'farm_preview-container'}>
                <span>Farm: {farm.name}</span>
                <span>at: {farm.address}</span>
                {isAdmin && <div className={'farm_actions'}>
                    <Button warning to={Urls.UPDATE_FARM_URL(farm.id)}>Edit</Button>
                    <Button danger onClick={() => handleModal(farm.id)}>Delete</Button>
                </div>}
            </div>
            <Modal displayModal={displayModal}
                   closeModal={handleModalClosing}
                   header={'Delete Farm'}
                   footer={modalActions()}
                   danger>
                Are you sure you want to delete this farm?
            </Modal>

        </Fragment>
    );
};

export default Farm;