import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";
import {fetchAccountFarms, fetchFarms, fetchUserAccessibleFarms} from "../../../../store/actions/farms/farmActions";
import CometSpinner from "../../commons/Spinner/CometSpinner";
import Farm from "./Farm/Farm";
import Button from "../../commons/form/Button/Button";
import Urls from "../../../../utils/urls";

import './Farms.css';

const Farms = (props) => {
        const dispatch = useDispatch();
        const {loading, farms, isAdmin} = useSelector(state => ({
            loading: state.farms.loading,
            error: state.farms.error,
            farms: state.farms.farms,
            isAdmin: state.auth.roles && state.auth.roles.includes('ADMIN')
        }));
        const accountId = useParams().accountId;

        useEffect(() => {
                if (accountId) {
                    dispatch(fetchAccountFarms(accountId));
                } else if (props.available) {
                    dispatch(fetchUserAccessibleFarms());
                } else {
                    dispatch(fetchFarms());
                }
                // eslint-disable-next-line react-hooks/exhaustive-deps
            }, []
        );

        const renderFarms = () => {
            const farmCollection = Object.values(farms);
            if (farmCollection.length === 0) {
                return <h1>No Farms Available!</h1>
            }
            return farmCollection.map(farm => (
                <Farm key={farm.id} farm={farm} isAdmin={isAdmin}/>
            ));
        }

        if (loading || !farms) {
            return <CometSpinner display={loading}/>
        }

        return (
            <div className={'farms_container'}>
                {accountId && isAdmin && <Button to={Urls.CREATE_FARM_URL(accountId)} success>Add Farm</Button>}
                <div className={'farms_inner-container'}>
                    {renderFarms()}
                </div>
            </div>
        );
    }
;

export default Farms;