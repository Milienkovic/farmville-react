import React from 'react';
import Farms from "./Farms";

const AvailableFarms = () => {
    return (
        <Farms available/>
    );
};

export default AvailableFarms;