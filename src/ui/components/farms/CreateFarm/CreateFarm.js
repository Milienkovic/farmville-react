import React, {Fragment} from 'react';
import FarmForm from "../FarmForm/FarmForm";

const CreateFarm = () => {
    return (
        <Fragment>
            <FarmForm/>
        </Fragment>
    );
};

export default CreateFarm;