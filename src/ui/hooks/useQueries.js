export const useQueries = (url, queryParam) => {
    return new URL(url).searchParams.get(queryParam);
}