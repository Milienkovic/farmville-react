import {useCallback} from "react";
import jwtDecode from 'jwt-decode';

export const useToken = () => {
    const AUTH_TOKEN = 'auth';

    const getAuthToken = useCallback(() => {
        return localStorage.getItem(AUTH_TOKEN);
    }, []);

    const storeAuthToken = useCallback((token) => {
        if (token)
            localStorage.setItem(AUTH_TOKEN, token);
    }, []);

    const decodeAuthToken = useCallback((token) => {
        if (token)
            return jwtDecode(token);
    }, []);

    const calculateExpiryDate = useCallback((expiresInMilis) => {
        return new Date(new Date().getMilliseconds() + expiresInMilis);
    }, []);

    const isTokenStored = useCallback(() => {
        return !!localStorage.getItem(AUTH_TOKEN);
    }, []);

    const isTokenValid = useCallback((token) => {
        if (token) {
            const decoded = jwtDecode(token);
            const now = Date.now();
            return decoded.exp > now / 1000;
        }
    }, []);

    return {storeAuthToken, getAuthToken, decodeAuthToken, calculateExpiryDate, isTokenStored, isTokenValid};
}