import axios from './../../../utils/axios-instance';
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {FarmTypes} from "./farmTypes";
import Endpoints from "../../../utils/Endpoints";

const fetchFarmsStart = () => actionStart(FarmTypes.FETCH_FARMS_START);
const fetchFarmsSuccess = farms => actionSuccess(FarmTypes.FETCH_FARMS_SUCCESS, 'farms', farms);
const fetchAccountFarmsSuccess = farms => actionSuccess(FarmTypes.FETCH_ACCOUNT_FARMS_SUCCESS, 'farms', farms);
const fetchUserAccessibleFarmsSuccess = farms => actionSuccess(FarmTypes.FETCH_USER_ACCESSIBLE_FARMS_SUCCESS, 'farms', farms);
const fetchFarmsFail = error => actionFail(FarmTypes.FETCH_FARMS_FAIL, error);

const fetchFarmStart = () => actionStart(FarmTypes.FETCH_FARM_START);
const fetchFarmSuccess = farm => actionSuccess(FarmTypes.FETCH_FARM_SUCCESS, 'farm', farm);
const fetchFarmFail = error => actionFail(FarmTypes.FETCH_FARM_FAIL, error);

const mutateFarmStart = () => actionStart(FarmTypes.MUTATE_FARM_START);
const createFarmSuccess = farm => actionSuccess(FarmTypes.CREATE_FARM_SUCCESS, 'farm', farm);
const updateFarmSuccess = farm => actionSuccess(FarmTypes.UPDATE_FARM_SUCCESS, 'farm', farm);
const deleteFarmSuccess = farmId => actionSuccess(FarmTypes.DELETE_FARM_SUCCESS, 'id', farmId);
const mutateFarmFail = (error, fieldErrors) => actionFail(FarmTypes.MUTATE_FARM_FAIL, error, fieldErrors);

export const fetchFarms = () => {
    return dispatch => {
        dispatch(fetchFarmsStart());
        axios.get(Endpoints.Farms.fetchFarms())
            .then(res => {
                dispatch(fetchFarmsSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFarmsFail(err.response.data.message || 'Fetching failed'));
            });
    }
}

export const fetchUserAccessibleFarms = () => {
    return dispatch => {
        dispatch(fetchFarmsStart());
        axios.get(Endpoints.Farms.fetchUserAvailableFarms())
            .then(res => {
                dispatch(fetchUserAccessibleFarmsSuccess(res.data))
            })
            .catch(err => {
                dispatch(fetchFarmsFail(err.response.data.message || 'Fetching failed'));
            });
    }
}

export const fetchAccountFarms = accountId => {
    return dispatch => {
        dispatch(fetchFarmsStart())
        axios.get(Endpoints.Farms.fetchAccountFarms(accountId))
            .then(res => {
                dispatch(fetchAccountFarmsSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchFarmsFail(err.response.data.message || 'Fetching failed!'));
            });
    }
}

export const fetchFarm = farmId => {
    return dispatch => {
        dispatch(fetchFarmStart());
        axios.get(Endpoints.Farms.fetchFarm(farmId))
            .then(res => {
                dispatch(fetchFarmSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchFarmFail(err.response.data.message || 'Fetching failed'));
            });
    }
}

export const createFarm = (accountId, farmData, history, redirectTo) => {
    return dispatch => {
        dispatch(mutateFarmStart());
        axios.post(Endpoints.Farms.createFarm(accountId), farmData)
            .then(res => {
                dispatch(createFarmSuccess(res.data));
                history.push(redirectTo);
            })
            .catch(err => {
                dispatch(mutateFarmFail(err.response.data.message || 'Creating failed', err.response.data.fieldErrors));
            });
    }
}

export const updateFarm = (farmId, farmData, history, redirectTo) => {
    return dispatch => {
        dispatch(mutateFarmStart());
        axios.patch(Endpoints.Farms.updateFarm(farmId), farmData)
            .then(res => {
                dispatch(updateFarmSuccess(res.data));
                history.push(redirectTo);
            })
            .catch(err => {
                dispatch(mutateFarmFail(err.response.data.message || 'Updating failed', err.response.data.fieldErrors));
            });
    }
}

export const deleteFarm = farmId => {
    return dispatch => {
        dispatch(mutateFarmStart());
        axios.delete(Endpoints.Farms.deleteFarm(farmId))
            .then(res => {
                console.log(res.data);
                dispatch(deleteFarmSuccess(farmId));
            })
            .catch(err => {
                dispatch(mutateFarmFail(err.response.data.message || 'Creating failed'));
            });
    }
}
