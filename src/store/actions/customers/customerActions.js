import axios from './../../../utils/axios-instance';
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {CustomerTypes} from "./customerTypes";
import Endpoints from "../../../utils/Endpoints";
import Urls from "../../../utils/urls";

const fetchCustomersStart = () => actionStart(CustomerTypes.FETCH_CUSTOMERS_START);
const fetchCustomersSuccess = customers => actionSuccess(CustomerTypes.FETCH_CUSTOMERS_SUCCESS, 'customers', customers);
const fetchCustomersFail = error => actionFail(CustomerTypes.FETCH_CUSTOMERS_FAIL, error);

const fetchCustomerStart = () => actionStart(CustomerTypes.FETCH_CUSTOMER_START);
const fetchCustomerSuccess = customer => actionSuccess(CustomerTypes.FETCH_CUSTOMER_SUCCESS, 'customer', customer);
const fetchCustomerFail = error => actionFail(CustomerTypes.FETCH_CUSTOMER_FAIL, error);

const mutateCustomerStart = () => actionStart(CustomerTypes.MUTATE_CUSTOMER_START);
const createCustomerSuccess = customer => actionSuccess(CustomerTypes.CREATE_CUSTOMER_SUCCESS, 'customer', customer);
const updateCustomerSuccess = customer => actionSuccess(CustomerTypes.UPDATE_CUSTOMER_SUCCESS, 'customer', customer);
const deleteCustomerSuccess = customerId => actionSuccess(CustomerTypes.DELETE_CUSTOMER_SUCCESS, 'id', customerId);
const mutateCustomerFail = (error, fieldErrors) => actionFail(CustomerTypes.MUTATE_CUSTOMER_FAIL, error, fieldErrors);

export const fetchCustomers = () => {
    return dispatch => {
        dispatch(fetchCustomersStart());
        axios.get(Endpoints.Customers.fetchCustomers())
            .then(res => {
                dispatch(fetchCustomersSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchCustomersFail(err.response.data.message || 'Fetching failed'));
            })
    }
}

export const fetchCustomer = customerId => {
    return dispatch => {
        dispatch(fetchCustomerStart());
        axios.get(Endpoints.Customers.fetchCustomer(customerId))
            .then(res => {
                dispatch(fetchCustomerSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchCustomerFail(err.response.data.message || 'Fetching failed'));
            })
    }
}

export const createCustomer = (customerData, history) => {
    return dispatch => {
        dispatch(mutateCustomerStart());
        axios.post(Endpoints.Customers.createCustomer(), customerData)
            .then(res => {
                dispatch(createCustomerSuccess(res.data));
                history.push(Urls.HOME);
            })
            .catch(err => {
                dispatch(mutateCustomerFail(err.response.data.message || 'Creating failed', err.response.data.fieldErrors));
            });
    }
}

export const updateCustomer = (customerId, customerData, history,redirecTo) => {
    return dispatch => {
        dispatch(mutateCustomerStart());
        axios.patch(Endpoints.Customers.updateCustomer(customerId), customerData)
            .then(res => {
                dispatch(updateCustomerSuccess(res.data));
                history.push(redirecTo)
            })
            .catch(err => {
                dispatch(mutateCustomerFail(err.response.data.message || 'Updating failed', err.response.data.fieldErrors));
            });
    }
}

export const deleteCustomer = customerId => {
    return dispatch => {
        dispatch(mutateCustomerStart());
        axios.delete(Endpoints.Customers.deleteCustomer(customerId))
            .then(res => {
                console.log(res.data);
                dispatch(deleteCustomerSuccess(customerId));
            })
            .catch(err => {
                dispatch(mutateCustomerFail(err.response.data.message || 'Deleting failed'));
            });
    }
}