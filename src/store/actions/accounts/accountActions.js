import axios from './../../../utils/axios-instance';
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {AccountTypes} from "./accountTypes";
import Endpoints from "../../../utils/Endpoints";

const fetchAccountsStart = () => actionStart(AccountTypes.FETCH_ACCOUNTS_START);
const fetchAccountsSuccess = accounts => actionSuccess(AccountTypes.FETCH_ACCOUNTS_SUCCESS, 'accounts', accounts);
const fetchUserAccessibleAccountsSuccess = accounts => actionSuccess(AccountTypes.FETCH_USER_ACCESSIBLE_ACCOUNTS_SUCCESS, 'accounts', accounts);
const fetchAccountsFail = error => actionFail(AccountTypes.FETCH_ACCOUNTS_FAIL, error);

const fetchAccountStart = () => actionStart(AccountTypes.FETCH_ACCOUNT_START);
const fetchAccountSuccess = account => actionSuccess(AccountTypes.FETCH_ACCOUNT_SUCCESS, 'account', account);
const fetchAccountFail = error => actionFail(AccountTypes.FETCH_ACCOUNT_FAIL, error);

const mutateAccountStart = () => actionStart(AccountTypes.MUTATE_ACCOUNT_START);
const grandAccessSuccess = message => actionSuccess(AccountTypes.GRANT_ACCESS_SUCCESS, 'message', message);
const revokeAccessSuccess = message => actionSuccess(AccountTypes.REVOKE_ACCESS_SUCCESS, 'message', message);
const mutateAccountFail = (error, fieldErrors) => actionFail(AccountTypes.MUTATE_ACCOUNT_FAIL, error, fieldErrors);

export const fetchAccounts = () => {
    return dispatch => {
        dispatch(fetchAccountsStart());
        axios.get(Endpoints.Accounts.fetchAccounts())
            .then(res => {
                console.log(res.data)
                dispatch(fetchAccountsSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchAccountsFail(err.response.data.message || 'Fetching failed'));
            });
    }
}
export const fetchUserAccessibleAccounts = () => {
    return dispatch => {
        dispatch(fetchAccountsStart());
        axios.get(Endpoints.Accounts.fetchUserAvailableAccounts())
            .then(res => {
                dispatch(fetchUserAccessibleAccountsSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchAccountsFail(err.response.data.message || 'Fetching failed'));
            });
    }
}
export const fetchAccount = accountId => {
    return dispatch => {
        dispatch(fetchAccountStart());
        axios.get(Endpoints.Accounts.fetchAccount(accountId))
            .then(res => {
                dispatch(fetchAccountSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchAccountFail(err.response.data.message || 'Fetching failed'));
            });
    }
}

export const grantAccess = (accountId) => {
    return dispatch => {
        dispatch(mutateAccountStart());
        axios.post(Endpoints.Accounts.grantAccountAccess(accountId))
            .then(res => {
                dispatch(grandAccessSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateAccountFail(err.response.data.message || 'Action failed'));
            });
    }
}

export const revokeAccess = (accountId) => {
    return dispatch => {
        dispatch(mutateAccountStart());
        axios.post(Endpoints.Accounts.revokeAccountAccess(accountId))
            .then(res => {
                dispatch(revokeAccessSuccess(res.data));
            })
            .catch(err => {
                dispatch(mutateAccountFail(err.response.data.message || 'Action failed'));
            });
    }
}

