import axios from './../../../utils/axios-instance';
import Endpoints from "../../../utils/Endpoints";
import {AuthTypes} from "./authTypes";
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import Urls from "../../../utils/urls";

const AUTH_TOKEN = 'auth';

const loginStart = () => actionStart(AuthTypes.AUTH_START);
export const loginSuccess = token => actionSuccess(AuthTypes.AUTH_SUCCESS, 'token', token);
const loginFail = (error, fieldErrors) => actionFail(AuthTypes.AUTH_FAIL, error, fieldErrors);

const registrationStart = () => actionStart(AuthTypes.REGISTRATION_START);
const registrationSuccess = message => actionSuccess(AuthTypes.REGISTRATION_SUCCESS, 'message', message);
const registrationFail = (error, fieldErrors) => actionFail(AuthTypes.REGISTRATION_FAIL, error, fieldErrors);

const logoutSuccess = () => actionSuccess(AuthTypes.LOGOUT_SUCCESS, 'token', null);

export const login = loginData => {
    return dispatch => {
        dispatch(loginStart());
        axios.post(Endpoints.Auth.login(), loginData)
            .then(res => {
                console.log(res.data);
                const {authorization} = res.headers;
                localStorage.setItem(AUTH_TOKEN, authorization);
                dispatch(loginSuccess(authorization));
            })
            .catch(err => {
                dispatch(loginFail(err.response.data.message || 'Login Failed', err.response.data.fieldErrors));
            });
    }
}

export const registration = (registrationData, history) => {
    return dispatch => {
        dispatch(registrationStart());
        axios.post(Endpoints.Auth.registration(), registrationData)
            .then(res => {
                console.log(res.data);
                dispatch(registrationSuccess(res.data));
                history.push(Urls.HOME);
            })
            .catch(err => {
                dispatch(registrationFail(err.response.data.message || 'Registration Failed', err.response.data.fieldErrors));
            });
    }
}

export const logout = () => {
    return dispatch => {
        localStorage.removeItem(AUTH_TOKEN);
        dispatch(logoutSuccess());
    }
}
