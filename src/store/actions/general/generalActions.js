import {actionStart} from "../utils/actions";
import GeneralTypes from "./generalTypes";

export const purgeErrors = () => actionStart(GeneralTypes.PURGE_ERRORS);