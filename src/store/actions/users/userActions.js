import axios from './../../../utils/axios-instance';
import Endpoints from "../../../utils/Endpoints";
import {actionFail, actionStart, actionSuccess} from "../utils/actions";
import {UserTypes} from "./userTypes";
import Urls from "../../../utils/urls";
import {logout} from "../auth/authActions";


const updateUserStart = () => actionStart(UserTypes.MUTATE_USER_START);
const updateUserFail = (error, fieldErrors) => actionFail(UserTypes.MUTATE_USER_FAIL, error, fieldErrors);
const updateUserSuccess = user => actionSuccess(UserTypes.UPDATE_USER_SUCCESS, 'user', user);

const fetchUserStart = () => actionStart(UserTypes.FETCH_USER_START);
const fetchUserSuccess = user => actionSuccess(UserTypes.FETCH_USER_SUCCESS, 'user', user);
const fetchUserFail = (error, fieldErrors) => actionFail(UserTypes.FETCH_USER_FAIL, error, fieldErrors);

const fetchUsersStart = () => actionStart(UserTypes.FETCH_USERS_START);
const fetchUsersSuccess = users => actionSuccess(UserTypes.FETCH_USERS_SUCCESS, 'users', users);
const fetchUsersFail = (error, fieldErrors) => actionFail(UserTypes.FETCH_USERS_FAIL, error, fieldErrors);

const deleteUserStart = () => actionStart(UserTypes.MUTATE_USER_START);
const deleteUserSuccess = id => actionSuccess(UserTypes.DELETE_USER_SUCCESS, 'id', id);
const deleteUserFail = error => actionFail(UserTypes.DELETE_USER_SUCCESS, error);

export const fetchUsers = () => {
    return dispatch => {
        dispatch(fetchUsersStart());
        axios.get(Endpoints.Users.fetchUsers())
            .then(res => {
                dispatch(fetchUsersSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchUsersFail(err.response.data.message || 'Fetching failed'));
            });
    }
}

export const fetchUser = userId => {
    return dispatch => {
        dispatch(fetchUserStart());
        axios.get(Endpoints.Users.fetchUser(userId))
            .then(res => {
                dispatch(fetchUserSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchUserFail(err.response.data.message || 'Fetching failed'));
            });
    }
}

export const fetchUserByEmail = email => {
    return dispatch => {
        dispatch(fetchUserStart());
        axios.get(Endpoints.Users.fetchUserByEmail(email))
            .then(res => {
                dispatch(fetchUserSuccess(res.data));
            })
            .catch(err => {
                dispatch(fetchUserFail(err.response.data.message || 'Fetching failed'));
            });
    }

}

export const updateUser = (userId, userData, history) => {
    return dispatch => {
        dispatch(updateUserStart());
        axios.patch(Endpoints.Users.updateUser(userId), userData)
            .then(res => {
                console.log(res.data);
                dispatch(updateUserSuccess(res.data));
                history.push(Urls.HOME);
            })
            .catch(err => {
                dispatch(updateUserFail(err.response.data.message || 'User update failed', err.response.data.fieldErrors));
            });
    }
}

export const deleteUser = (userId, history) => {
    return dispatch => {
        dispatch(deleteUserStart());
        axios.delete(Endpoints.Users.deleteUser(userId))
            .then(res => {
                console.log(res.data);
                dispatch(deleteUserSuccess(userId));
                dispatch(logout());
                history.push(Urls.HOME);
            })
            .catch(err => {
                dispatch(deleteUserFail(err.response.data.message || 'Deleting failed'));
            })
    }

}