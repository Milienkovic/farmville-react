import {AccountTypes} from "../actions/accounts/accountTypes";
import {actionFail, actionStart, actionSuccess} from "./utils/reducerActions";
import _ from 'lodash';

const initState = {
    loading: false,
    account: null,
    accounts: null,
    error: null,
    fieldErrors: null,
    message: null,
    availableAccounts: null
}

const fetchAccountStart = (state, action) => actionStart(state, action);
const fetchAccountSuccess = (state, action) => actionSuccess(state, action, 'account', action.account);
const fetchAccountFail = (state, action) => actionFail(state, action);

const fetchAccountsStart = (state, action) => actionStart(state, action);
const fetchAccountsSuccess = (state, action) => actionSuccess(state, action, 'accounts', _.mapKeys(action.accounts, 'id'));
const fetchUserAccessibleAccountsSuccess = (state, action) => actionSuccess(state, action, 'availableAccounts', _.mapKeys(action.accounts, 'id'));
const fetchAccountsFail = (state, action) => actionFail(state, action);

const mutateAccountStart = (state, action) => actionStart(state, action);
const grantAccessSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);
const revokeAccessSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);
const mutateAccountFail = (state, action) => actionFail(state, action);


export default function accountReducer(state = initState, action) {
    switch (action.type) {
        case AccountTypes.FETCH_ACCOUNT_START:
            return fetchAccountStart(state, action);
        case AccountTypes.FETCH_ACCOUNT_SUCCESS:
            return fetchAccountSuccess(state, action);
        case AccountTypes.FETCH_ACCOUNT_FAIL:
            return fetchAccountFail(state, action);
        case AccountTypes.FETCH_ACCOUNTS_START:
            return fetchAccountsStart(state, action);
        case AccountTypes.FETCH_ACCOUNTS_SUCCESS:
            return fetchAccountsSuccess(state, action);
        case AccountTypes.FETCH_USER_ACCESSIBLE_ACCOUNTS_SUCCESS:
            return fetchUserAccessibleAccountsSuccess(state, action);
        case AccountTypes.FETCH_ACCOUNTS_FAIL:
            return fetchAccountsFail(state, action);
        case AccountTypes.MUTATE_ACCOUNT_START:
            return mutateAccountStart(state, action);
        case AccountTypes.GRANT_ACCESS_SUCCESS:
            return grantAccessSuccess(state, action);
        case AccountTypes.REVOKE_ACCESS_SUCCESS:
            return revokeAccessSuccess(state, action);
        case AccountTypes.MUTATE_ACCOUNT_FAIL:
            return mutateAccountFail(state, action);
        default:
            return state;
    }
}