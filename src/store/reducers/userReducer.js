import {UserTypes} from "../actions/users/userTypes";
import _ from 'lodash';
import {actionFail, actionStart, actionSuccess, purgeErrors} from "./utils/reducerActions";
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    loading: false,
    user: null,
    users: null,
    error: null,
    fieldErrors: null,
    message: null,
    deletedId: null
}

const fetchUserStart = (state, action) => actionStart(state, action);
const fetchUserSuccess = (state, action) => actionSuccess(state, action, 'user', action.user);
const fetchUserFail = (state, action) => actionFail(state, action);

const fetchUsersStart = (state, action) => actionStart(state, action);
const fetchUsersSuccess = (state, action) => actionSuccess(state, action, 'users', _.mapKeys(action.users, 'id'));
const fetchUsersFail = (state, action) => actionFail(state, action);

const mutateUserStart = (state, action) => actionStart(state, action);
const updateUserSuccess = (state, action) => actionSuccess(state, action, 'user', action.user);
const deleteUserSuccess = (state, action) => {
    return{
        ...state,
        loading: false,
        users: _.omit(state.users, action.id),
        deletedId: action.id
    }
}
const mutateUserFail = (state, action) => actionFail(state, action);

const purgeUserErrors = (state, action) => purgeErrors(state, action);

export default function (state = initState, action) {

    switch (action.type) {
        case UserTypes.FETCH_USER_START:
            return fetchUsersStart(state, action);
        case UserTypes.FETCH_USER_SUCCESS:
            return fetchUserSuccess(state, action);
        case UserTypes.FETCH_USER_FAIL:
            return fetchUserFail(state, action);
        case UserTypes.FETCH_USERS_START:
            return fetchUserStart(state, action);
        case UserTypes.FETCH_USERS_SUCCESS:
            return fetchUsersSuccess(state, action);
        case UserTypes.FETCH_USERS_FAIL:
            return fetchUsersFail(state, action);
        case UserTypes.MUTATE_USER_START:
            return mutateUserStart(state, action);
        case UserTypes.UPDATE_USER_SUCCESS:
            return updateUserSuccess(state, action);
        case UserTypes.DELETE_USER_SUCCESS:
            return deleteUserSuccess(state, action);
        case UserTypes.MUTATE_USER_FAIL:
            return mutateUserFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeUserErrors(state, action)
        default:
            return state;
    }
}