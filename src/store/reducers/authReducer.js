import {AuthTypes} from "../actions/auth/authTypes";
import {actionFail, actionStart, actionSuccess} from "./utils/reducerActions";
import jwtDecode from 'jwt-decode';

const initState = {
    loading: false,
    token: null,
    expiresIn: null,
    roles: null,
    error: null,
    fieldErrors: null,
    message: null
}

const authStart = (state, action) => actionStart(state, action);
const authSuccess = (state, action) => {
    const decoded = jwtDecode(action.token);
    console.log(decoded);
    const expiresIn = Date.now() + decoded.exp / 1000;
    const roles = decoded.privileges;
    return {
        ...state,
        loading: false,
        token: action.token,
        expiresIn,
        roles
    }
};
const authFail = (state, action) => actionFail(state, action);

const registrationStart = (state, action) => actionStart(state, action);
const registrationSuccess = (state, action) => actionSuccess(state, action, 'message', action.message);
const registrationFail = (state, action) => actionFail(state, action);

const logoutSuccess = (state, action) =>{
    return{
        ...state,
        loading: false,
        token: action.token,
        roles: null,
        expiresIn: null,
    }
}
export default function authReducer(state = initState, action) {
    switch (action.type) {
        case AuthTypes.AUTH_START:
            return authStart(state, action);
        case AuthTypes.AUTH_SUCCESS:
            return authSuccess(state, action);
        case AuthTypes.AUTH_FAIL:
            return authFail(state, action);
        case AuthTypes.REGISTRATION_START:
            return registrationStart(state, action);
        case AuthTypes.REGISTRATION_SUCCESS:
            return registrationSuccess(state, action);
        case AuthTypes.REGISTRATION_FAIL:
            return registrationFail(state, action);
        case AuthTypes.LOGOUT_SUCCESS:
            return logoutSuccess(state, action);
        default:
            return state;
    }
}