import {combineReducers} from "redux";
import authReducer from "./authReducer";
import userReducer from "./userReducer";
import accountReducer from "./accountReducer";
import customerReducer from "./customerReducer";
import farmReducer from "./farmReducer";

export const rootReducer = combineReducers({
    auth: authReducer,
    users: userReducer,
    accounts: accountReducer,
    customers: customerReducer,
    farms: farmReducer
});