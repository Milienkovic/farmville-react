import {FarmTypes} from "../actions/farms/farmTypes";
import {actionFail, actionStart, actionSuccess, purgeErrors} from "./utils/reducerActions";
import _ from 'lodash';
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    loading: false,
    farm: null,
    farms: null,
    error: null,
    fieldErrors: null,
    message: null,
    deletedId: null
}

const fetchFarmStart = (state, action) => actionStart(state, action);
const fetchFarmSuccess = (state, action) => actionSuccess(state, action, 'farm', action.farm);
const fetchFarmFail = (state, action) => actionFail(state, action);

const fetchFarmsStart = (state, action) => actionStart(state, action);
const fetchFarmsSuccess = (state, action) => actionSuccess(state, action, 'farms', _.mapKeys(action.farms, 'id'));
const fetchAccountFarmsSuccess = (state, action) => actionSuccess(state, action, 'farms', _.mapKeys(action.farms, 'id'));
const fetchUserAccessibleFarmsSuccess = (state, action) => actionSuccess(state, action, 'farms', _.mapKeys(action.farms, 'id'));
const fetchFarmsFail = (state, action) => actionFail(state, action);

const mutateFarmStart = (state, action) => actionStart(state, action);
const createFarmSuccess = (state, action) => actionSuccess(state, action, 'farm', action.farm);
const updateFarmSuccess = (state, action) => actionSuccess(state, action, 'farm', action.farm);
const deleteFarmSuccess = (state, action) => {
    return{
        ...state,
        deletedId: action.id,
        loading: false,
        farms: _.omit(state.farms, action.id)
    }
};
const mutateFarmFail = (state, action) => actionFail(state, action);

const purgeFarmErrors = (state, action) => purgeErrors(state, action);

export default function farmReducer(state = initState, action) {
    switch (action.type) {
        case FarmTypes.FETCH_FARM_START:
            return fetchFarmStart(state, action);
        case FarmTypes.FETCH_FARM_SUCCESS:
            return fetchFarmSuccess(state, action);
        case FarmTypes.FETCH_FARM_FAIL:
            return fetchFarmFail(state, action);
        case FarmTypes.FETCH_FARMS_START:
            return fetchFarmsStart(state, action);
        case FarmTypes.FETCH_FARMS_SUCCESS:
            return fetchFarmsSuccess(state, action);
        case FarmTypes.FETCH_ACCOUNT_FARMS_SUCCESS:
            return fetchAccountFarmsSuccess(state, action);
        case FarmTypes.FETCH_USER_ACCESSIBLE_FARMS_SUCCESS:
            return fetchUserAccessibleFarmsSuccess(state, action);
        case FarmTypes.FETCH_FARMS_FAIL:
            return fetchFarmsFail(state, action);
        case FarmTypes.MUTATE_FARM_START:
            return mutateFarmStart(state, action);
        case FarmTypes.CREATE_FARM_SUCCESS:
            return createFarmSuccess(state, action);
        case FarmTypes.UPDATE_FARM_SUCCESS:
            return updateFarmSuccess(state, action);
        case FarmTypes.DELETE_FARM_SUCCESS:
            return deleteFarmSuccess(state, action);
        case FarmTypes.MUTATE_FARM_FAIL:
            return mutateFarmFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeFarmErrors(state, action);
        default:
            return state;
    }
}