export const actionStart = (state, action) => {
    return {
        ...state,
        loading: true,
        error: null,
        fieldErrors: null,
        message: null
    };
}

export const actionSuccess = (state, action, propName, propValue) => {
    return {
        ...state,
        loading: false,
        [propName]: propValue
    };
}

export const actionFail = (state, action) =>{
    return{
        ...state,
        loading: false,
        error: action.error,
        fieldErrors: action.fieldErrors
    }
}

export const purgeErrors = (state, action) =>{
    return{
        ...state,
        error: null,
        fieldErrors: null,
        message: null
    }
}