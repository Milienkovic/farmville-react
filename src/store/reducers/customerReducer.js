import {CustomerTypes} from "../actions/customers/customerTypes";
import {actionFail, actionStart, actionSuccess, purgeErrors} from "./utils/reducerActions";
import _ from 'lodash';
import GeneralTypes from "../actions/general/generalTypes";

const initState = {
    loading: false,
    customer: null,
    customers: null,
    error: null,
    fieldErrors: null,
    message: null,
    deletedId: null
}

const fetchCustomerStart = (state, action) => actionStart(state, action);
const fetchCustomerSuccess = (state, action) => actionSuccess(state, action, 'customer', action.customer);
const fetchCustomerFail = (state, action) => actionFail(state, action);

const fetchCustomersStart = (state, action) => actionStart(state, action);
const fetchCustomersSuccess = (state, action) => actionSuccess(state, action, 'customers', _.mapKeys(action.customers, 'id'));
const fetchCustomersFail = (state, action) => actionFail(state, action);

const mutateCustomerStart = (state, action) => actionStart(state, action);
const createCustomerSuccess = (state, action) => actionSuccess(state, action, 'customer', action.customer);
const updateCustomerSuccess = (state, action) => actionSuccess(state, action, 'customer', action.customer);
const deleteCustomerSuccess = (state, action) => {
    return {
        ...state,
        loading: false,
        deletedId: action.id,
        customers: _.omit(state.customers, action.id)
    }
}
const mutateCustomerFail = (state, action) => actionFail(state, action);

const purgeCustomerErrors = (state, action) => purgeErrors(state, action);

export default function customerReducer(state = initState, action) {

    switch (action.type) {
        case CustomerTypes.FETCH_CUSTOMER_START:
            return fetchCustomerStart(state, action);
        case CustomerTypes.FETCH_CUSTOMER_SUCCESS:
            return fetchCustomerSuccess(state, action);
        case CustomerTypes.FETCH_CUSTOMER_FAIL:
            return fetchCustomerFail(state, action);
        case CustomerTypes.FETCH_CUSTOMERS_START:
            return fetchCustomersStart(state, action);
        case CustomerTypes.FETCH_CUSTOMERS_SUCCESS:
            return fetchCustomersSuccess(state, action);
        case CustomerTypes.FETCH_CUSTOMERS_FAIL:
            return fetchCustomersFail(state, action);
        case CustomerTypes.MUTATE_CUSTOMER_START:
            return mutateCustomerStart(state, action);
        case CustomerTypes.UPDATE_CUSTOMER_SUCCESS:
            return updateCustomerSuccess(state, action);
        case CustomerTypes.CREATE_CUSTOMER_SUCCESS:
            return createCustomerSuccess(state, action);
        case CustomerTypes.DELETE_CUSTOMER_SUCCESS:
            return deleteCustomerSuccess(state, action);
        case CustomerTypes.MUTATE_CUSTOMER_FAIL:
            return mutateCustomerFail(state, action);
        case GeneralTypes.PURGE_ERRORS:
            return purgeCustomerErrors(state, action);
        default:
            return state;
    }
}