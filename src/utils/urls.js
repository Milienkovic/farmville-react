export default class Urls {
    static HOME = '/';

    static LOGIN = '/login';
    static LOGOUT = '/logout';
    static REGISTRATION = '/registration';

    static ADMIN_PANEL = '/adminPanel';

    static USERS = '/users';
    static UPDATE_USER = '/updateUser';

    static CUSTOMERS = '/customers';
    static CREATE_CUSTOMER = '/createCustomer';
    static UPDATE_CUSTOMER='/updateCustomer/:customerId';
    static UPDATE_CUSTOMER_URL= customerId=> `/updateCustomer/${customerId}`;

    static FARMS = '/farms';
    static AVAILABLE_FARMS = '/farms/available';
    static CREATE_FARM = '/createFarm/:accountId';
    static UPDATE_FARM = '/updateFarm/:farmId';
    static UPDATE_FARM_URL = farmId => `/updateFarm/${farmId}`;

    static CREATE_FARM_URL = accountId => `/createFarm/${accountId}`;

    static ACCOUNT_FARMS = '/accounts/farms/:accountId';
    static ACCOUNT_FARMS_URL = accountId => `/accounts/farms/${accountId}`;

    static ACCOUNTS = '/accounts';
    static ACCOUNTS_TESTING = '/accounts/testing';
    static AVAILABLE_ACCOUNTS = '/accounts/available';
}