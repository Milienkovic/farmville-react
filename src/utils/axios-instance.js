import axios from 'axios';

const instance = axios.create();
const requestHandler = request => {
    const token = localStorage.getItem('auth');
    if (token) {
        request.headers['Authorization'] = token;
    }
    return request;
}

instance.interceptors.request.use(request => requestHandler(request));

export default instance;