export default class Endpoints {
    static Auth = class {
        static login = () => '/login';
        static registration = () => '/registration';
    }

    static Users = class {
        static fetchUsers = () => '/users';
        static fetchUser = userId => `/users?id=${userId}`;
        static fetchUserByEmail = email => `/users?email=${email}`;
        static updateUser = userId => `/users?id=${userId}`;
        static deleteUser = userId => `/users?id=${userId}`;
    }

    static Customers = class {
        static fetchCustomers = () => '/customers';
        static fetchCustomer = customerId => `/customers?id=${customerId}`;
        static createCustomer = () => '/customers';
        static updateCustomer = customerId => `/customers?id=${customerId}`;
        static deleteCustomer = customerId => `/customers?id=${customerId}`;
    }

    static Farms = class {
        static fetchFarms = () => '/farms';
        static fetchAccountFarms = accountId => `/farms?account=${accountId}`;
        static fetchFarm = farmId => `/farms?id=${farmId}`;
        static createFarm = accountId => `/farms?account=${accountId}`;
        static updateFarm = farmId => `/farms?id=${farmId}`;
        static deleteFarm = farmId => `/farms?id=${farmId}`;
        static fetchUserAvailableFarms = () => `/farms/available`;
    }

    static Accounts = class {
        static fetchAccounts = () => '/accounts';
        static fetchAccount = accountId => `/accounts?id=${accountId}`;
        static fetchUserAvailableAccounts = () => `/accounts/available`;
        static grantAccountAccess = (accountId) => `/accounts/grant?id=${accountId}`;
        static revokeAccountAccess = (accountId) => `/accounts/revoke?id=${accountId}`;
    }
}